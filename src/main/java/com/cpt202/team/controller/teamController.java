package com.cpt202.team.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.ArrayList;
import com.cpt202.team.models.Team;

@RestController
@RequestMapping("/team")
public class TeamController {

    private List<Team> teams = new ArrayList<Team>();

    @GetMapping("/list")
    public List<Team> getList(){
        return teams;
    }

    @PostMapping("/add")
    public void addTeam(@RequestBody Team team){
        teams.add(team);
    }
}
